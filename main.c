//http://www.electronicwings.com




#define F_CPU 8000000UL			/* Define CPU Frequency e.g. here 8MHz */
#include <avr/io.h>			/* Include AVR std. library file */
#include <util/delay.h>			/* Include Delay header file */
#include <stdio.h>
#define LCD_Dir  DDRD			/* Define LCD data port direction */
#define LCD_Port PORTD			/* Define LCD data port */
#define RS PD0				/* Define Register Select pin */
#define EN PD1 

void LCD_Command( unsigned char cmnd )
{
	LCD_Port = (LCD_Port & 0x0F) | (cmnd & 0xF0); /* sending upper nibble */
	LCD_Port &= ~ (1<<RS);		/* RS=0, command reg. */
	LCD_Port |= (1<<EN);		/* Enable pulse */
	_delay_us(1);
	LCD_Port &= ~ (1<<EN);

	_delay_us(200);

	LCD_Port = (LCD_Port & 0x0F) | (cmnd << 4);  /* sending lower nibble */
	LCD_Port |= (1<<EN);
	_delay_us(1);
	LCD_Port &= ~ (1<<EN);
	_delay_ms(2);
}


void LCD_Char( unsigned char data )
{
	LCD_Port = (LCD_Port & 0x0F) | (data & 0xF0); /* sending upper nibble */
	LCD_Port |= (1<<RS);		/* RS=1, data reg. */
	LCD_Port|= (1<<EN);
	_delay_us(1);
	LCD_Port &= ~ (1<<EN);

	_delay_us(200);

	LCD_Port = (LCD_Port & 0x0F) | (data << 4); /* sending lower nibble */
	LCD_Port |= (1<<EN);
	_delay_us(1);
	LCD_Port &= ~ (1<<EN);
	_delay_ms(2);
}

void LCD_Init (void)			/* LCD Initialize function */
{
	LCD_Dir = 0xFF;			/* Make LCD port direction as o/p */
	_delay_ms(20);			/* LCD Power ON delay always >15ms */
	
	LCD_Command(0x02);		/* send for 4 bit initialization of LCD  */
	LCD_Command(0x28);              /* 2 line, 5*7 matrix in 4-bit mode */
	LCD_Command(0x0c);              /* Display on cursor off*/
	LCD_Command(0x06);              /* Increment cursor (shift cursor to right)*/
	LCD_Command(0x01);              /* Clear display screen*/
	_delay_ms(2);
}


void LCD_String (char *str)		/* Send string to LCD function */
{
	int i;
	for(i=0;str[i]!=0;i++)		/* Send each char of string till the NULL */
	{
		LCD_Char (str[i]);
	}
}

void LCD_String_xy (char row, char pos, char *str)	/* Send string to LCD with xy position */
{
	if (row == 0 && pos<16)
	LCD_Command((pos & 0x0F)|0x80);	/* Command of first row and required position<16 */
	else if (row == 1 && pos<16)
	LCD_Command((pos & 0x0F)|0xC0);	/* Command of first row and required position<16 */
	LCD_String(str);		/* Call LCD string function */
}

void LCD_Clear()
{
	LCD_Command (0x01);		/* Clear display */
	_delay_ms(2);
	LCD_Command (0x80);		/* Cursor at home position */
}

void Write_Mask_LCD()
{
LCD_String_xy(0,0,"Motor: ");
LCD_String_xy(1,0,"Time:");
LCD_String_xy(1,8,"min");
}

void Update_LCD(int set,int f)
{
char sf[4];
char setf[6];
//Convert Integers to strings
sprintf(sf, "%d", f);
sprintf(setf,"%d", set);

//Write First Line

if(f < 10) {
	LCD_String_xy(0,6,sf);
	LCD_String_xy(0,7,"%");
	LCD_String_xy(0,8,"   ");
} 
else if((f >= 10) &  (f < 100)) {
	LCD_String_xy(0,6,sf);
	LCD_String_xy(0,8,"%");
	LCD_String_xy(0,9,"   ");
}
else if(f == 100){
	LCD_String_xy(0,6,"100");
	LCD_String_xy(0,9,"%");
	LCD_String_xy(0,10,"   ");
}
else{}

//Write Second Line

//time first

if(set < 10) {
	LCD_String_xy(1,5,setf);
	LCD_String_xy(1,6," ");
} 
else if(set >= 10) {
	LCD_String_xy(1,5,setf);
}


else{}

}

void initADC0()
{
 
ADMUX =
             (1 << ADLAR) |     // left shift result
             (1 << REFS1) |     // Sets ref. voltage to VCC, bit 1
             (1 << REFS0) |     // Sets ref. voltage to VCC, bit 0
             (0 << MUX3)  |     // use ADC0 for input (PC0), MUX bit 3
             (0 << MUX2)  |     // use ADC0 for input (PC0), MUX bit 2
             (0 << MUX1)  |     // use ADC0 for input (PC0), MUX bit 1
             (0 << MUX0);       // use ADC0 for input (PC0), MUX bit 0
 
ADCSRA =
             (1 << ADEN)  |     // Enable ADC 
             (1 << ADPS2) |     // set prescaler to 64, bit 2 
             (1 << ADPS1) |     // set prescaler to 64, bit 1 
             (0 << ADPS0);      // set prescaler to 64, bit 0  

}




int main()
{
int on = 0;
int16_t time = 0;
int set = 15;
int f = 0;
int count = 0;
float step = 0.395; //Scaler for 256 to 0-100%
DDRD = 0b11111111;	// Set Data Direction Register to out
DDRB |= (1<<PB0); 	// PB0 is output
DDRC &= ~(1 << PC3);    // PC3 is Input
PORTC |= (1 << PC3);    // enable pull-up resistor

//PWM on Timer2
DDRB |= (1 << DDB3);
// PB3 is now an output
OCR2 = 0;
// set PWM for 50% duty cycle
TCCR2 |= (1 << COM21);
// set none-inverting mode
TCCR2 |= (1 << WGM21) | (1 << WGM20);
// set fast PWM Mode
TCCR2 |= (1 << CS21);
// set prescaler to 8 and starts PWM

initADC0();

LCD_Init();

LCD_Clear();

Write_Mask_LCD();
while(1)
{
initADC0();
_delay_ms(1);
ADCSRA |= (1 << ADSC);         // start ADC measurement
while (ADCSRA & (1 << ADSC) ); // wait till conversion complete 

if (PINC & (1 << PC3)) {
	count--;
	f = (ADCH*step);		//Read ADC scale result and write to Displayvariable
}
else if (on == 0) {
	count++;
	set = (ADCH*step);		//Read ADC scale result and write to Displayvariable
	if (set > 99){set = 99;}
}
else{}

if(count < 0)
	count = 0;

if(count == 30){
	count = 0;			
	on = 1;
}

if(on == 1){
	OCR2 = ADCH; 			//Set Motorspeed
	time++;
}

Update_LCD(set,f);
if((time > 2430) && (on == 1))
{
	time = 0;
	set--;
	if(set == 0){
	PORTB |= (1 << PB0); // PB0 goes high
        on = 0;
	OCR2 = 0;	
	}
}



	}
}
 
